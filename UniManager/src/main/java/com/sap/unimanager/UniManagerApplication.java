package com.sap.unimanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniManagerApplication.class, args);
    }

}
